import { Application, Request, Response, NextFunction} from 'express';
import express from 'express';
import {connect, NatsConnectionOptions, Payload} from 'ts-nats';
import bodyParser from "body-parser";
const port = Number(process.env.PORT || 3100);
console.log(port)
const server_nat = String(process.env.SERVER_NAT || '0.0.0.0');
const logRequest = (req: Request, res: Response, next: NextFunction) => {
  console.log(req);
  next();
}

const app: Application = express();
//app.use(logRequest);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get( "/", (req, res) => {
    res.send( "API Express ALIVE" );
});
app.use(require('./controller'));

app.listen(port, () => {
    console.log(`server started on port ${port}`);
});

async function connect_nat_server(server: String) {
  try {
    console.log("Try to connected to "+'nats://'+server+':4222')
    let nc = await connect({servers: ['nats://'+server+':4222']});
    // Do something with the connection
    console.log("Connected")
    let sub = await nc.subscribe('server_up', (err, msg) => {
      if(err) {
          console.error("Some error sub Nat")
      } else 
          console.log('Get message server_up queue',msg)
      });
  } catch(ex) {
    // handle the error
    console.log(ex)
  }
}

connect_nat_server(server_nat)