# Control de versiones

En este documento tiene como objetivo mostrar de un groso modo el funcionamiento de git, sus comandos y la plataforma Gitlab para almacenar nuestro repositorio en la nube.

**Indice**

- [Control de versiones](#control-de-versiones)
  * [Crear un repositorio en Gitlab](#crear-un-repositorio-en-gitlab)
    + [Actualizar cambios](#actualizar-cambios)
    + [Incluir Readme.md](#incluir-readmemd)
    + [Visualizar cambios](#visualizar-cambios)
    + [Ignorar algunos ficheros](#ignorar-algunos-ficheros)
  * [Uitilidades](#uitilidades)
    + [Visualizar el estado del git en el Host](#visualizar-el-estado-del-git-en-el-host)
    + [Crear y cambiar de rama](#crear-y-cambiar-de-rama)
      - [Actualizar master](#actualizar-master)
  * [Interfaz Gitlab](#interfaz-gitlab)
    + [Gestión de miembros del repositorio](#gesti-n-de-miembros-del-repositorio)
    + [Añadir nuevo Issue](#a-adir-nuevo-issue)
    + [Ver estado del proyecto sobre un commit](#ver-estado-del-proyecto-sobre-un-commit)
  * [Errores](#errores)
      - [Error común en Windows](#error-com-n-en-windows)
  * [Más información](#m-s-informaci-n)



## Crear un repositorio en Gitlab

<img src="img/1-git.png" style="zoom:80%;" />

<img src="img/cap1.png" style="zoom:80%;" />

Una vez creado,  es hora de crear un repositorio Git de nuestro proyecto actual:

<img src="img/cap2.png" style="zoom:80%;" />

En este caso seguir los comandas expuestos en la imagine anterior, donde pone **Push an existing folder**:

1. Inicializar el proyecto git en la raíz de nuestro proyecto, referenciamos en repositorio Gitlab creado y añadimos todos los ficheros ( git add .)
	 <img src="img/cap3.png" style="zoom:80%;" />
2. Subimos nuestro proyecto ( git local) al repositorio Gitlab
	<img src="img/cap3-b.png" style="zoom:80%;" />

Cuando se realiza `git push -u origin master`, significa que estamos pasando nuestro proyecto o los nuevos cambios a la rama principal de Gitlab (rama principal = master).

Ver el resultado en Gitlab:

<img src="img/cap3-c.png" style="zoom:67%;" />



### Actualizar cambios

Una vez instanciado el git del proyecto (*git init*) , cuando se desea actualizar los cambios nuevos que se han ido realizado solo hay que realizar 3 pasos:

1. Establecer o añadir los cambios
2. Añadir un nuevo commit
3. Subir el commit al Gitlab

**Lista de comandos Git:**

```powershell
➜  Git git:(master) ✗ git add .
➜  Git git:(master) ✗ git commit -m "add update" 
[master b1f4976] add update
 4 files changed, 65 insertions(+), 3 deletions(-)
➜  Git git:(master) git push origin master
```

Nota: Se puede realizar el paso 1 y 2 varias veces sin tener que subirlo al Gitlab, cuando se realice posteriormente el **push** se añadirán todos los commits no añadidos al repositorio de Gitlab

### Incluir Readme.md

En la raíz del proyecto se suele incluir un fichero con el formato Readme.md (tipo Markdown) donde es bueno incluir información del estilo:

- Resumen del proyecto
- Paso de instalación
- Dependencias
- Referencias
- Ejemplo de lanzamiento

Cuando es subido al repositorio creado, automáticamente es renderizado y visualizado en el Gitlab. Para manejarse mejor con este tipo de formato Markdown se recomienda utilizar el programa [Typora](https://typora.io/)

### Visualizar cambios

Añadimos en el fichero *installer-node.sh* (existente en el último commit) una nueva linea `info "[INSTALLER] new version"`.

Podemos visualizar las "*diferencias*" entre nuestro código actual y el código del último commit, en este caso, el último commit es igual al código existente en el repositorio de Gitlab.

<img src="img/git-status.png" style="zoom:67%;" />

Esta captura es el resultado de ejecutar `git status`, este indica los cambios realizados desde el último commit.

Podemos además ver los cambios realizados con `git diff`:

<img src="img/git-diff.png" style="zoom:80%;" />



### Ignorar algunos ficheros

Este apartado es importante, hay que tener en cuenta que cuando se lanza el comando `git add .` este subo todo el contenido de la capeta, es decir, se subirán fichero de configuración, credenciales y ficheros ocultos. Es importante conocer que ficheros son esenciales para nuestro proyecto y evitar subir contenido innecesario. Aquí algunas razones:

- Tamaño excesivo del repositorio
- Ficheros de configuración que pueden ocasionar problemas si se clona el repositorio en otro equipo
- Exponer datos sensibles (contraseñas, cuentas etc ...)

A git se le puede indicar que archivos no deseamos que se suban cuando realizamos un **push**.  Para ello, se debe crear un fichero llamado **.gitignore** en la raíz del proyecto.

Ejemplo:

```powershell
➜  Git git:(master) ✗ la
total 32K
drwxrwxr-x 8 manu manu 4.0K Mar 15 12:48 .git
-rw-rw-r-- 1 manu manu   17 Mar 15 13:09 .gitignore
drwxrwxr-x 2 manu manu 4.0K Mar 15 12:42 img
-rw-rw-r-- 1 manu manu  211 Mar 15 13:11 .metadata
-rw-rw-r-- 1 manu manu   22 Mar 15 13:10 mykeys
drwxrwxr-x 4 manu manu 4.0K Mar 15 12:15 Project-example
-rw-rw-r-- 1 manu manu 4.3K Mar 15 13:00 Readme.md
```

Viendo el contenido del proyecto, no deseamos subir los ficheros *.metada* y *keys*, por tanto, se crea y añade al fichero .gitignore dichos ficheros. El contenido del .gitignore sería el siguiente:

```js
mykeys
.metadata
# to avoid in subfolders
**/mykeys
**/.metada
```

Resultado en Gitlab:

<img src="img/git-ginore.png" style="zoom:80%;" />

## Uitilidades

### Visualizar el estado del git en el Host

`git --no-pager log --oneline`

<img src="img/git-logs.png" style="zoom:80%;" />

En la captura anterior se observa como:

1. Obtener los logs sin actualizar
2. Actualizar el repositorio
3. De nuevo, se obtienen los logs

Podemos observar que ahora el resultado de volver a realizar `git --no-pager log --oneline` es distinto:

```bash
6352344 (HEAD -> master) Changed Readme.md
7a8e61e (origin/master) new update
7484229 Initial commit
```

Es importante entender que ocurre aquí, en este caso tenemos nuestro HEAD con master apuntando al nuevo commit (*Changed Readme.md)*, pero en la segunda linea pone (origin/master) new update. Esto quiere decir que nuestro repositorio Gitlab (Cloud) esta ahora mismo des-actualizado, por tanto debemos de realizar el push correspondiente:

```powershell
➜  Git git:(master) ✗ git push -u origin master

➜  Git git:(master) ✗ git --no-pager log --oneline
6352344 (HEAD -> master, origin/master) Changed Readme.md
7a8e61e new update
7484229 Initial commit
```

Ahora en Gitlab se encuentra totalmente actualizado

### Crear y cambiar de rama

Crear una nueva rama, significa poder modificar nuestro código y actualizar el git sin tener que afectar al código del master. Se debe de entender como un git secundario que puede ser modificado si afectar al master y en un futuro estos cambios ser añadidos en la rama principal (master).

Actualmente nuestro git apunto al master (HEAD -> master )

```powershell
❯ git --no-pager log --oneline
0e8385b (HEAD -> master, origin/master) updated readme
13900df add .gitignore
6352344 Changed Readme.md
7a8e61e new update
7484229 Initial commit
```

Creamos una nueva rama y vemos el estado:

```powershell
❯ git --no-pager branch -a
* master
  remotes/origin/master
❯ git checkout -b mi-rama
Switched to a new branch 'mi-rama'
❯ git --no-pager log --oneline
0e8385b (HEAD -> mi-rama, origin/master, master) updated readme
13900df add .gitignore
6352344 Changed Readme.md
7a8e61e new update
7484229 Initial commit
```

Vemos como ahora nuestro git en el proyecto apunta a la nueva rama (HEAD -> mi-rama), si aplicamos los cambios en nuestro git del proyecto:

```powershell
❯ git add .
❯ git commit -m "updated rama mi-rama"
[mi-rama 54b3c04] updated rama mi-rama
 4 files changed, 5 insertions(+), 1 deletion(-)
 create mode 100644 img/commit_branches.png
 create mode 100644 img/commit_branches2.png
 create mode 100644 img/commit_branches3.png
❯ git --no-pager log --oneline
54b3c04 (HEAD -> mi-rama) updated rama mi-rama
0e8385b (origin/master, master) updated readme
13900df add .gitignore
6352344 Changed Readme.md
7a8e61e new update
7484229 Initial commit
```

Ahora, la rama principal (master) se ha mantenido en el estado anterios, mientras que la rama **mi-rama** ha cambiado.  Subimos la nueva rama al Gitlab ( `❯ git push -u origin mi-rama` ):

![](img/mi-rama-gitlab.png)



#### Actualizar master

Llegará un punto donde se desea actualizar el master con los cambios de la rama, en este punto hay que tener en cuenta lo siguiente:

> En caso de que algún fichero haya sido modificado tanto en el master como en la rama, git mostrará un error, por tanto hay que indicarle que versión del fichero queremos establecer los cambios o directamente hacer un rebase o merge

Diferencias entre rebase y merge en el siguiente enlace: [merging vs rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

Para actualizar el master es necesario realizar los siguientes pasos:

1. Cambiar de nuevo a la rama master
2. Realizar un merge de la rama mi-rama
3. Actualizar el repositorio Gitlab

Estos pasos son realizados en las siguientes imágenes:

<img src="img/update-master-from-mi-rama-gitlab.png" style="zoom:80%;" />

<img src="img/2update-master-from-mi-rama-gitlab.png" style="zoom:80%;" />



Si vemos de nuevo el estado de nuestro git del equipo:

```powershell
❯ git --no-pager log --oneline
6f48fe7 (HEAD -> master, origin/master) updated master
ef300cb updated master
c0601a1 (origin/mi-rama, mi-rama) updated rama mi-rama
54b3c04 updated rama mi-rama
0e8385b updated readme
13900df add .gitignore
6352344 Changed Readme.md
7a8e61e new update
7484229 Initial commit
```

Ahora nuestro git del proyecto apunto de nuevo al master y este esta actualizado junto al repositorio de Gitlab.

#### Actualizar proyecto Local a la versión Gitlab

reset --hard: Descarta todos los ficheros modificados desde el último commit del proyecto Local.
```
git reset --hard
git push
```
## Interfaz Gitlab

En este apartado son mostrados las principales ventanas de Gitlab, enfocado sobretodos en las ventana de mayor utilidad para gestionar el repositorio.

### Gestión de miembros del repositorio

Es posible añadir más miembros al repositorio creado con un rol específico, para más información sobre los roles acceder al siguiente enlace: [Permissions](https://docs.gitlab.com/ee/user/permissions.html)

![](img/Gitlab-members.png)

### Añadir nuevo Issue

Los issues son muy útiles para tener una lista de tareas referentes al proyecto, donde varios miembros del mismo pueden comentar y compartir información. Además se mantiene un registro en el propio repositorios de los issues generados, cerrados, más comentados etc...

![](img/add-issue.png)



### Ver estado del proyecto sobre un commit

Esto es útil cuando se detecta un error en un commit más reciente y se desea obtener el proyecto de un commit anterior estable.

![](img/comits-gitlab.png)

En esta captura muestra una serie de commits realizado. Si se quiere ver el contenido completo (proyecto) de un commit deseado, acceder al icono de la carpeta localizada en el lado derecho del commit correspondiente.



## Errores

#### Error común en Windows

Si cuando realizamos git add . y sale un mensaje parecido a este:

```
PS C:\Users\Manu\Desktop\ITBA_BACKUP> git init
Initialized empty Git repository in C:/Users/Manu/Desktop/ITBA_BACKUP/.git/
PS C:\Users\Manu\Desktop\ITBA_BACKUP> git add .
warning: LF will be replaced by CRLF in Persistence JDBC/paw-2019a-6/Bancalet/.project.
The file will have its original line endings in your working directory.
warning: LF will be replaced by CRLF in Persistence JDBC/paw-2019a-6/Bancalet/.settings/org.eclipse.m2e.core.prefs.
The file will have its original line endings in your working directory.
```

Esto es debido a un problema con los saltos de linea, por anto, solo se debe indicar a git que ignore dichas lineas:

```
git config core.autocrlf false
## Re-do it again: git add .
```




## Más información

[Git Cheat Sheet](docs/gitcom.md)

[Gitlab Git Cheat Sheet](docs/git-cheat-sheet.pdf)
